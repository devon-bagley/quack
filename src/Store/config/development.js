import { createStore, applyMiddleware, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
// import tools from 'redux-devtools';
// import promiseMiddleware from 'redux-promise';
import Logger from 'redux-logger';
import rootEpic from '../epics';

const epicMiddleware = createEpicMiddleware(rootEpic);

const middleware = [
    epicMiddleware,
    Logger,
];

// By default we try to read the key from ?debug_session=<key> in the address bar
const getDebugSessionKey = function () {
  const matches = window.location.href.match(/[?&]debug_session=([^&]+)\b/);
  return (matches && matches.length) ? matches[1] : null;
};

const enhancer = compose(
    applyMiddleware(...middleware),
    // window.devToolsExtension ? window.devToolsExtension() : DevTools.instrument(),
    // Optional. Lets you write ?debug_session=<key> in address bar to persist debug sessions
    // tools.persistState(getDebugSessionKey())
);

export default function configureStore(rootReducerMod, initialState) {
    const store = createStore(rootReducerMod, initialState, enhancer);

    // Enable hot module replacement for reducers
    // (requires Webpack or Browserify HMR to be enabled)
    // if (module.hot) {
    //   module.hot.accept('store', () => {
    //     const nextReducer = buildRootReducer();
    //     store.replaceReducer(nextReducer);
    //   });
    // }

    return store;
}
