import { createReducer } from 'redux-act';
import Actions from '../actions';
import setDeepKey from '../../Utility/setDeepKey';
import deepMerge from '../../Utility/deepMerge';

export function reducerAddReducer(state, payload) {
  return setDeepKey(state, payload.key, payload.value);
}

export function reducerRemoveReducer(state, payload) {
  return setDeepKey(state, payload, undefined);
}

export function reducerMergeReducer(state, payload) {
  return deepMerge(state, payload);
}

export function moduleAddReducer(state, payload) {
  return deepMerge(state, { modules: { [payload.name]: payload.internalReducer || function() {} } });
}

export default function internalReducer(defaultState = {}) {
  return createReducer({
    [Actions.reducer.add]: reducerAddReducer,
    [Actions.reducer.remove]: reducerRemoveReducer,
    [Actions.reducer.merge]: reducerMergeReducer,
    [Actions.module.add]: moduleAddReducer,
  }, defaultState);
}
