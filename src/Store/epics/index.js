import { combineEpics } from 'redux-observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/mergeMap';
import epicActions from '../actions/epics';
import reducerActions from '../actions/reducers';

export default function createEpic() {
  const epic$ = new BehaviorSubject(combineEpics(
    (action$) => action$.ofType(epicActions.add.toString())
      .do(a => setImmediate(() => nextEpic(a.payload)))
      .map(() => epicActions.added('Epic added!')),
    (action$) => action$.ofType(reducerActions.add.toString())
      .map(() => reducerActions.added('Reducer added!')),
    (action$) => action$.ofType(reducerActions.merge.toString())
      .map(() => reducerActions.merged('Reducer collection merged!')),
    (action$) => action$.ofType(reducerActions.remove.toString())
      .map(() => reducerActions.removed('Reducer removed.'))
  ));

  function nextEpic(epic) {
    epic$.next(epic);
  }

  return function rootEpic(action$, store) {
    return epic$.mergeMap((epic) => {
      return epic(action$, store);
    });
  };
}
