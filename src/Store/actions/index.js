import reducer from './reducers';
import epic from './epics';
import module from './modules';

export default {
  reducer,
  epic,
  module,
};
