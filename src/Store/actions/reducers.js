import { createAction } from 'redux-act';

export const addReducer = createAction(
  'Command: Add reducer to collection.',
  (key, value) => ({ key, value })
);
export const removeReducer = createAction('Command: Remove a reducer.');
export const removedReducer = createAction('Result: Removed reducer.');
export const mergeReducer = createAction('Command: Merge reducer collection.');
export const addedReducer = createAction('Result: Added reducer to collection.');
export const mergedReducer = createAction('Result: Merged reducer collection.');

export default {
  add: addReducer,
  added: addedReducer,
  merge: mergeReducer,
  merged: mergedReducer,
  remove: removeReducer,
  removed: removedReducer,
};
