import { createAction } from 'redux-act';

export const addModule = createAction('Command: Load module to store.');
export const addedModule = createAction('Result: Added new module.');

export default {
  add: addModule,
  added: addedModule,
};
