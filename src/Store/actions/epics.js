import { createAction } from 'redux-act';

export const addEpic = createAction('Command: Add epic to chain.');
export const addedEpic = createAction('Result: Epic added to chain.');

export default {
  add: addEpic,
  added: addedEpic,
}
