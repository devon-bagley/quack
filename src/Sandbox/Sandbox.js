import Logger from '../Utility/logger';
const PropMap = new WeakMap();

export default class Sandbox {

  constructor(core, instanceId, options, moduleId) {
    core._mediator.installTo(this);
    PropMap.set(this, {
      instanceId,
      options,
      moduleId,
      console: Logger.log(`Quack.${moduleId}.${instanceId}`),
    });
  }

  get instanceId() {
    return PropMap.get(this).instanceId;
  }

  get moduleId() {
    return PropMap.get(this).moduleId;
  }

  get options() {
    return PropMap.get(this).options;
  }

  get console() {
    return PropMap.get(this).console;
  }

}
