import { createAction, createReducer } from 'redux-act';
import Sandbox from './Sandbox';

const CoreMap = new WeakMap();

export default class StoreSandbox extends Sandbox {

  constructor(core, instanceId, options, moduleId) {
    super(core, instanceId, options, moduleId);
    CoreMap.set(this, core);
  }

  get state() {
    return CoreMap.get(this).getState();
  }

  get actions() {
    return CoreMap.get(this).actions;
  }

  createAction(key, ...args) {
    return CoreMap.get(this)
      .addAction(key, createAction(...args));
  }

  createReducer(key, ...args) {
    const reducer = createReducer(...args);
    CoreMap.get(this).addReducer(key, reducer);
    return reducer;
  }

  addEpic(epic) {
    CoreMap.get(this).addEpic(epic);
    return epic;
  }

  subscribe(callback) {
    return CoreMap.get(this).subscribe(callback);
  }

}
