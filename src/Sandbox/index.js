import Sandbox from './Sandbox';
import StoreSandbox from './StoreSandbox';

export default {
  Sandbox,
  StoreSandbox,
}
