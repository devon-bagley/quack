import Logger from '../Utility/logger';

export default class Module {

  constructor(sandbox) {
    this.sandbox = sandbox;
  }

  init() {
    this.sandbox.console.info("Initialized");
  }

  destroy() {
    this.sandbox.console.info("Destroyed");
  }

}
