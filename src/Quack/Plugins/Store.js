import { createStore, applyMiddleware } from 'redux';
import { combineEpics, createEpicMiddleware } from 'redux-observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/auditTime';

import internalReducer from '../../Store/reducers';
import internalEpic from '../../Store/epics';
import Actions from '../../Store/actions';

import deepCombine from '../../Utility/deepCombine';
import setDeepKey from '../../Utility/setDeepKey';

let subscriptions = [];
let pending = [];

const callSubscriptions = update => subscriptions.forEach(sub => sub(update));
const removeSubscription = item => {
  subscriptions = subscriptions.filter(i => i !== item);
};
const addSubscription = item => {
  subscriptions = [
    ...subscriptions,
    item,
  ];
};

const addPending = action => {
  pending = [
    ...(pending || []),
    action,
  ];

  return pending;
};

const destroyPending = () => {
  const old = [...(pending || [])];
  pending = [];
  return old;
};

const internal = createStore(
  internalReducer(),
  applyMiddleware(createEpicMiddleware(internalEpic()))
);

// The behavior subject allows the dynamic loading of epics.
const createEpic = core => // new BehaviorSubject(combineEpics(
  ($action, store) => $action.do(a => addPending(a))
    .auditTime(40)
    .map(a => ({
      state: store.getState(),
      actions: destroyPending()
    }))
    .do(a => {
      core.emit('update', a);
      callSubscriptions(a);
    }).filter(item => false)
// ));

export default function StorePlugin(core, options = {}) {
  // const $epic = createEpic(core);

  const main = createStore(
    (state = {}) => state || {},
    applyMiddleware(
      createEpicMiddleware(
        internalEpic()
      )
    )
  );

  core.addEpic = epic => main.dispatch(Actions.epic.add.raw(epic));
  core.addReducer = (key, reducer) => internal.dispatch(
    Actions.reducer.add(key, reducer)
  );
  core.actions = {};
  core.addAction = (name, generator) => {
    const action = generator.bindTo(main.dispatch);

    core.actions = {
      ...core.actions,
      [name]: action,
    };

    return action;
  };

  Object.keys(main).forEach(key => {
    core[key] = main[key];
  });

  core.subscribe = callback => {
    const wrapper = update => callback(update.state, ...update.actions);

    addSubscription(wrapper);
    return () => removeSubscription(wrapper);
  };

  internal.subscribe(
    () => main.replaceReducer(deepCombine(internal.getState()))
  );

  core.addEpic(createEpic(core));

  return main;
}
