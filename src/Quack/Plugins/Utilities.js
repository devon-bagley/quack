const UniqueIds = new WeakMap();

export default class Utilities {
  constructor(core) {
    this.mixin(this, core);
  }

  uniqueId(length = 8) {
    let unique = UniqueIds.get(this) || [];
    let id = "";

    while(unique.indexOf(id) >= 0 || id.length < length) {
      id = "";
      while(id.length < length) {
        id += Math.random().toString(36).substr(2);
      }
    }

    unique = [ ...unique, id ];
    UniqueIds.set(this, unique);

    return id;
  }

  clone(data) {
    if (Array.isArray(data)) {
      return [ ...data.map(i => this.clone(i)) ];
    }

    if (typeof data === 'object' && !(data instanceof Function)) {
      let next = {};
      
      Object.keys(data).forEach(key => {
        next = {
          ...next,
          [key]: this.clone(data[key]),
        };
      });
      
      return next;
    }

    return data;
  }

  mixin(giver, receiver, override = false) {
    Object.keys(giver).forEach(key => {
      if (override || !(receiver[key])) {
        let value = giver[key];
        if(typeof value === 'function') {
          value = value.bind(giver);
        }
        receiver[key] = value;
      }
    });
  };
}