export default class List {
  constructor(core) {
    core.lsInstances = () => this.ls(core._instances);
    core.lsModules = () => this.ls(core._modules);
    core.lsPlugins = () => core._plugins.filter(p => p.id).map(p => p.id)
  }

  ls(obj) {
    return Object.keys(obj);
  }
}