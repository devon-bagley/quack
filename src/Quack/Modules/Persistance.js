import ls from 'local-storage';
import Module from '../../Module';
import setDeepKey from '../../Utility/setDeepKey';

export default class PersistanceModule extends Module {

  constructor(sandbox) {
    super(sandbox);

    this.persistKeys = {};
  }

  init() {
    if (this.sandbox.options) {
      if (this.sandbox.options.persistance) {
        this.storageKey = this.sandbox.options.persistance.key || 'quackPersist';
        (this.sandbox.options.persistance.list || []).forEach(key => {
          this.persistKeys = {
            ...this.persistKeys,
            [key]: ls(this.storageKey)[key] || false,
          };
        });
      }
    }

    if (this.storageKey) {
      this.sandbox.store.replaceReducer(this.wrapReducer(this.sandbox.buildRootReducer()));

      this.sandbox.on('store.internal.update', (data) => {
        this.sandbox.store.replaceReducer(this.wrapReducer(data.reducer));
      });

      ls.on(this.storageKey, (value) => {
        this.persistKeys = value;
        this.forceUpdate = true;
        this.sandbox.store.dispatch({ type: '@@PERSISTANCE_UPDATE' });
      });
    }
  }

  wrapReducer(reducer) {
    return ((state, action) => {
      let next = {
        ...state,
        ...reducer(state, action),
      };

      Object.keys(this.persistKeys).forEach(key => {
        let value = this.getDeepValue(next, key);
        
        if (!value) {
          if (this.persistKeys[key]) {
            next = setDeepKey(next, key, this.persistKeys[key]);
          }
        } else {
          if (this.persistKeys[key] && this.forceUpdate) {
            next = setDeepKey(next, key, this.persistKeys[key]);
            value = this.persistKeys[key];
            this.forceUpdate = false;
          }
          this.persistKeys[key] = value;
        }
      });

      ls(this.storageKey, this.persistKeys);

      return next;
    }).bind(this);
  }

  getDeepValue(target, key) {
    const path = key.split('.');
    return path.reduce((prev, ck) => {
      if (!ck) {
        return prev;
      }

      return prev[ck];
    }, target);
  }

}