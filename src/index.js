import sa from './Utility/scaleApp';
import Sandbox from './Sandbox';
import Base from './Module';
import { Store } from './Quack/Plugins';

require("babel-polyfill");
require("setimmediate");

const Quack = new sa.Core(Sandbox.StoreSandbox);

Quack.use(Store);

export const SandBox = Sandbox.Sandbox;
export const Scale = sa;
export const Module = Base;

export default function createStore(...modules) {
  modules.forEach(module => Quack.register(module.name, module.creator, module.options));
  return Quack.start();
}
