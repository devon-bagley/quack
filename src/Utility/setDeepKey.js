import update from 'immutability-helper';

update.extend('$auto', function(value, object) {
  return object ?
    update(object, value):
    update({}, value);
});

update.extend('$autoArray', function(value, object) {
  return object ?
    update(object, value):
    update([], value);
});

export default function setDeepKey(target = {}, key, value) {
  const path = key.split('.');
  
  const done = path.reverse().reduce((prev, current, ci) => {
    if (ci === 0) {
      return { [current]: { $set: value } };
    }

    return { [current]: { $auto: prev } };
  }, {});
  // console.log('DONE', done);
  return update(target, done);
}
