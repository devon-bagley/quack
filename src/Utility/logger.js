import Logr from 'logr.js';

if (process.env.NODE_ENV === 'production') {
    Logr.setLevel(Logr.levels.NONE);
} else {
    Logr.setLevel(Logr.levels.ERROR);
}

export default Logr;
