import { combineReducers } from 'redux';

export default function deepCombine(collection) {
  const next = {};

  Object.keys(collection).forEach(key => {
    if (!(collection[key] instanceof Function)) {
      if (typeof collection[key] === 'object') {
        next[key] = deepCombine(collection[key]);
      }
    }

    if (collection[key] instanceof Function || typeof collection[key] === 'function') {
      next[key] = collection[key];
    }
  });

  return combineReducers(next);
}
