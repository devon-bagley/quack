import setDeepKey from './setDeepKey';

export default function deepMerge(target = {}, source = {}) {
  let paths = {};

  const pathAppend = (p, value) => {
    if (typeof value === 'object' && !(value instanceof Function)) {
      let newPaths = {};
      
      Object.keys(paths).filter(k => k !== p.split('.').slice(-1).join('.'))
        .forEach(key => {
          newPaths[key] = paths[key];
        });

      Object.keys(value).forEach(key => {
        pathAppend(`${p}.${key}`, value[key]);
      });
    } else {
      paths = {...paths, [p]: value};
    }
  }

  Object.keys(source).forEach(key => pathAppend(key, source[key]));
  
  let nextTarget = {...target};

  Object.keys(paths).forEach(key => {
    nextTarget = setDeepKey(nextTarget, key, paths[key]);
  });

  return {...target, ...nextTarget};
}
