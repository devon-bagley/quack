import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import uglify from 'rollup-plugin-uglify-es';

// rollup.config.js
export default {
  entry: 'src/index.js',
  format: 'umd',
  moduleName: 'Quack',
  plugins: [
    resolve(),
    commonjs(),
    babel({
      exclude: 'node_modules/**' // only transpile our source code
    }),
    ...(process.env.NODE_ENV === 'production' ? [uglify()] : [])
  ],
  dest: process.env.NODE_ENV === 'production' ? 'production.js' : 'development.js' // equivalent to --output
};
